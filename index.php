<?php
	require_once 'animal.php';
	require_once 'Frog.php';
	require_once 'Ape.php';
	$sheep = new Animal('shaun');
	$sungokong = new Ape('kera sakti');
	$kodok = new Frog('buduk');

	echo "$sheep->name <br>"; // "shaun"
	echo "$sheep->legs <br>"; // 2
	var_dump($sheep->cold_blooded);// false
	echo "<br> <br>";

	echo "$sungokong->name <br>";
	echo "$sungokong->legs <br>";
	$sungokong->yell(); // "Auooo"
	echo "<br> <br>";

	echo "$kodok->name <br>";
	echo "$kodok->legs <br>";
	$kodok->jump() ; // "hop hop"

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
?>